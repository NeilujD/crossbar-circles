# Crossbar Circles - by NeilujD #

I made this real-time web application using [**Crossbar**](http://crossbar.io/).
There is no revolution in there : it is just a simple demonstration of what you could do using Crossbar.
---

## Features ##

* Display the current sessions count
* Click on the page to make appear circles of different colors
* Each clients can see all others clients circles appearing in real-time
* Increment the sessions count each time a session joined
* Decrement the sessions count each time a session leaved
* Update the displayed sessions count every 2 seconds (just to be sure)

---

## How does it work ##

Once you are on the page, you are connected to the Crossbar server. Then each time you click on the page, your browser publish on a certain topic so the others connected browser (which have all subscribed to the same topic) receive the click position and display the circles.
I used the [Raphaël](http://raphaeljs.com/) lib to draw the circles.

---

## How to install ##

You need to have Python2.7 installed to use `pip`.

```
#!bash
$ git clone https://git@bitbucket.org/NeilujD/crossbar_circles.git
$ cd crossbar_circles
$ sudo pip install -r requirements.txt
```

---
## How to use ##
* Start Crossbar with `crossbar start` from the `crossbar_circles` directory
* Open a browser page on `http://localhost:8080/` address

---

### [Live demo](http://neilujd.com/crossbar-test/) ###
Also on [my post's blog](http://neilujd.com/crossbar-or-the-coffee-maker/) about Crossbar.